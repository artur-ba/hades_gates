from oslo_config import cfg
from oslo_log import log

import monasca_common.kafka.producer as kafka_producer
import monasca_common.kafka_lib.common as kafka_common

CONF = cfg.CONF

publisher_opt = [
    cfg.StrOpt('uri',
               required=True,
               help='Kafka Uri'),
    cfg.StrOpt('group',
               required=True,
               help='metric group'),
    cfg.IntOpt('max_retry',
               required=True,
               help='max retry'),
    cfg.IntOpt('wait_time',
               required=True,
               help='wait time'),
    cfg.BoolOpt('async',
                required=True,
                help='async'),
    cfg.BoolOpt('compact',
                required=True,
                help='compact'),
    cfg.IntOpt('partitions',
               required=True,
               help='partitions')
]

publisher_group = cfg.OptGroup(name='kafka', title='kafka')

cfg.CONF.register_group(publisher_group)
cfg.CONF.register_opts(publisher_opt, publisher_group)

LOG = log.getLogger(__name__)


class KafkaPublisher(object):
    def __init__(self, topic):
        if not cfg.CONF.kafka.uri:
            raise Exception('Missing uri in Kafka configuration')

        self.uri = cfg.CONF.kafka.uri
        self.topic = topic
        self.group = cfg.CONF.kafka.group
        self.wait_time = cfg.CONF.kafka.wait_time
        self.async = cfg.CONF.kafka.async
        self.max_retry = cfg.CONF.kafka.max_retry
        self.compact = cfg.CONF.kafka.compact
        self.partitions = cfg.CONF.kafka.partitions

        self._producer = kafka_producer.KafkaProducer(self.uri)

    def send_message(self, message):
        try:
            self._producer.publish(self.topic, message)
            LOG.info('Successfully published metric')
            LOG.info('Successfully published metric: {0}'.format(message))
        except (kafka_common.KafkaUnavailableError,
                kafka_common.LeaderNotAvailableError) as ex:
            LOG.error('Error while publishing to Kafka: {0}'.format(ex))
        except Exception as ex:
            LOG.error('Unknown error: {0}'.format(ex))
