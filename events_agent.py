import time
import ujson

from oslo_config import cfg
from oslo_log import log
import oslo_messaging

import kafka_publisher

CONF = cfg.CONF
LOG = log.getLogger(__name__)


class Endpoint(object):
    def info(self, ctxt, publisher_id, event_type, payload, metadata):
        self.__process_event(publisher_id, payload, event_type, metadata, 'INFO', ctxt)
        return oslo_messaging.NotificationResult.HANDLED

    def warn(self, ctxt, publisher_id, event_type, payload, metadata):
        self.__process_event(publisher_id, payload, event_type, metadata, 'WARNING', ctxt)
        return oslo_messaging.NotificationResult.HANDLED

    def error(self, ctxt, publisher_id, event_type, payload, metadata):
        self.__process_event(publisher_id, payload, event_type, metadata, 'ERROR', ctxt)
        return oslo_messaging.NotificationResult.HANDLED

    def __process_event(self, publisher_id, payload, event_type, metadata, severity, ctxt):
        # skip all .start events
        if '.start' in event_type:
            return None
        if 'nova_object.namespace' in payload.keys():
            LOG.debug('Processing nova object: {0}'.format(payload))
            create_metric_nova(payload=payload, metadata=metadata, events_type=event_type)
        elif 'image' in event_type:
            LOG.debug('Processing glance object: {0}'.format(payload))
            create_metric_glance(payload=payload, metadata=metadata, events_type=event_type)
        elif 'compute.instance' in event_type:
            LOG.debug('Processing nova compute object: {0}'.format(payload))
            create_metric_compute_nova(payload=payload, metadata=metadata, events_type=event_type)


def load_config(config='./events.conf'):
    CONF(args=[],
         project='events-test',
         default_config_files=[config])


def create_metric_compute_nova(payload, metadata, events_type):
    message = ''
    try:
        creation_time = get_timestamp(metadata['timestamp'])
        metric = {'meta': {'tenantId': payload['tenant_id'], 'region': 'CustomRegion'},
                  'creation_time': creation_time,
                  'metric': {
                      'timestamp': creation_time,
                      'value': 1,
                      'name': 'openstack.{0}'.format(events_type.split('.end')[0]),
                      'value_meta': {'sporadic': True, 'os_event': True},
                      'dimensions': {
                          'type': 'event',
                          'service': 'nova',
                          'instance_id': payload['instance_id'],
                          'hostname': payload['node'],
                      }
                  }
                  }
        message = ujson.dumps(metric)
        send_metric(message)
    except Exception as ex:
        LOG.error('Error while publishing metric fro nova: {0], error: {1}'.format(message, ex))


def create_metric_nova(payload, metadata, events_type):
    message = ''
    try:
        object_data = payload['nova_object.data']
        creation_time = get_timestamp(metadata['timestamp'])
        dimensions = {}
        if 'state_update' in object_data.keys():
            dimensions = {'state': object_data['state_update']['nova_object.data']['state'],
                          'old_state': object_data['state_update']['nova_object.data']['old_state']
                          }

        metric = {'meta': {'tenantId': object_data['tenant_id'], 'region': 'CustomRegion'},
                  'creation_time': creation_time,
                  'metric': {
                      'timestamp': creation_time,
                      'value': 1,
                      'name': "openstack.{0}".format(events_type.split('.end')[0]),
                      'value_meta': {'sporadic': True, 'os_event': True},
                      'dimensions': {
                                     'type': 'event',
                                     'service': 'nova',
                                     'instance_id': object_data['uuid'],
                                     'instance_name': object_data['display_name'],
                                     'hostname': object_data['node'],
                                     }
                  }
                  }
        metric['metric']['dimensions'].update(dimensions)
        message = ujson.dumps(metric)
        send_metric(message)
    except Exception as ex:
        LOG.error('Error while publishing metric from nova: {0}, error: {1}'.format(message, ex))


def get_timestamp(time_date):
    return time.mktime(time.strptime(time_date, '%Y-%m-%d %H:%M:%S.%f')) * 1000


def create_metric_glance(payload, metadata, events_type):
    message = ''
    try:
        creation_time = get_timestamp(metadata['timestamp'])
        # there are two types of glance object
        # one while creating a new image, second is triggered while creating a new instance
        metric = {'meta': {'tenantId': payload.get('owner', payload.get('receiver_tenant_id', '')),
                           'region': 'CustomRegion'},
                  'creation_time': creation_time,
                  'metric': {
                      'timestamp': creation_time,
                      'value': 1,
                      'name': 'openstack.{0}'.format(events_type.split('.end')[0]),
                      'value_meta': {'sporadic': True, 'os_event': True},
                      'diemnsions': {
                          'type': 'event',
                          'service': 'glance',
                          'image_id': payload.get('id', payload.get('image_id', '')),
                          'status': payload.get('status', ''),
                          'image_name': payload.get('name', ''),
                          'image_size': payload.get('size', payload.get('bytes_sent', ''))
                      }

                  }
                  }
        message = ujson.dumps(metric)
        send_metric(message)
    except Exception as ex:
        LOG.error('Error while publishing metric from glance: {0}, error: {1}'.format(message, ex))


def send_metric(metric):
    try:
        publisher.send_message(metric)
    except Exception as ex:
        LOG.error(ex)


log.set_defaults()
log.register_options(CONF)
load_config('/home/arturb/dev/monasca-events-agent/config.conf')
log.setup(CONF,
          product_name='events-agent',
          version='0.1')

publisher = kafka_publisher.KafkaPublisher('metrics')
transport = oslo_messaging.get_notification_transport(CONF)
targets = [oslo_messaging.Target(topic='monasca-workers', exchange='glance'),
           oslo_messaging.Target(topic='monasca-workers', exchange='nova')
           ]

endpoints = [Endpoint()]
pool = "monasca-workers"

server = oslo_messaging.get_notification_listener(transport, targets, endpoints, pool=pool)
LOG.info('Starting listener server....')
server.start()
server.wait()
